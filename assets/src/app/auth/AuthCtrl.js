/**
 * Module for handling authentication of users
 */

angular.module('dados.auth', [])
.controller('AuthCtrl', ['$scope', '$window', 'titleService', 'RestService', 
    /**
     * [AuthCtrl - controller for static login and registration pages]
     * @param {[type]} $scope
     * @param {[type]} $window
     * @param {[type]} titleService
     * @param {[type]} RestService
     */
    function ($scope, $window, titleService, RestService) {

        titleService.setTitle('Authorization Required');

        $scope.register = function(isValid) {
            if (isValid) {
                // extract role value from obj
                $scope.credentials.role = 'admin';
                RestService.post('/auth/local/register', $scope.credentials, function (err) {
                    console.log('ERROR at AuthCtrl');
                    console.log(err);
                    $scope.error = err;
                }).then(function (response) {
                    $window.location.href = '/';
                });
            }
        };

        $scope.login = function () {
            // post request to login
            RestService.post('auth/local', $scope.credentials, function (err) {
                $scope.error = 'Incorrect username or password';
            }).then(function (response) {
                $window.location.href = '/';
            });
        }
    }
]);