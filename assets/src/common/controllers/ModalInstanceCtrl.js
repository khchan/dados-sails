/**
 * Module for generic modal popups for queries on list items.
 * is loaded when $modal is opened from ModelCtrl.js
 */

angular.module('controller.modalinstance', ['controller.modeltable'])
.controller('ModalInstanceCtrl', 
    ['$scope', '$modalInstance', 'item', 'form', 'model', 'RestService', 'tableParams', 
    /**
     * [ModalInstanceCtrl - standard controller for modal popup]
     * @param {[type]} $scope
     * @param {[type]} $modalInstance
     * @param {[type]} item
     * @param {[type]} model
     * @param {[type]} RestService
     * @param {[type]} tableParams
     */
    function ($scope, $modalInstance, item, form, model, RestService, tableParams) {
        var editMode = false;
        $scope.item = item;
        $scope.model = model;
        $scope.tableParams = tableParams;
        $scope.newItem = {};
        console.log($scope.params);

        // if selected item from items row, pre-populate by binding params
        if (item) {
            var itemCopy = angular.copy(item);
            params = {};
            angular.forEach(itemCopy, function(value, key) {
                if (!(key.indexOf('$') == 0)) {
                    params[key] = value;
                }
            });
            $scope.params = params;
            editMode = true;
        }
        
        // if omitOnEdit field set to true in Question model and we are editing
        // remove field from DOM
        $scope.hideOmit = function(omitOnEdit) {
            return !(editMode && omitOnEdit);
        }

        /**
         * [submit - submits a valid form]
         * @param  {Boolean} isValid
         * @return {[type]}
         */
        $scope.submit = function(isValid) {
            if (isValid) {
                newItem = $scope.params;
                var promise = null,
                    submitError = function(err) {
                        console.log('ERROR at ModelInstanceCtrl');
                        console.log(err);
                        $scope.error = err;
                    };

                if (editMode)
                    promise = RestService.update($scope.model, $scope.params.id, newItem, submitError);
                else
                    promise = RestService.create($scope.model, newItem, submitError);
                
                promise.then(function(response) {
                    $scope.item = response.collection.items;
                    $modalInstance.close();
                    $scope.tableParams.reload();
                });
            } else {
                console.log('ERROR');
                $scope.error = 'Invalid Input';
            }        
        };

        /**
         * [close - closes a modal popup]
         * @return {[type]}
         */
        $scope.close = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);

