angular.module('dados.home', [])

.config(['$stateProvider',
	function ($stateProvider) {
		$stateProvider.state('home', {
			url: '/home',
			views: {
				'main': {
					controller: 'HomeCtrl',
					templateUrl: 'home/home.tpl.html'
				},
				'info@home': {
					controller: function ($scope) {
						$scope.test = "Hello World!";
					},
					template: 'info template with variable: {{test}}'
				}
			}
		});
	}
])
.controller('HomeCtrl', ['$scope', 'titleService',
	function ($scope, titleService) {
		titleService.setTitle('Home');
	}
]);