/**
 * Module for controlling the header on each page;
 * uses the Menus service to track and add menu items based on role
 */

angular.module('dados.header', [])
.run(['Menus', 
    function (Menus) {
        // Set top bar menu items
        Menus.addMenuItem('topbar', 'Header.MENU_ABOUT', 'about');
        Menus.addMenuItem('topbar', 'Header.MENU_USERS', 'users');
        Menus.addMenuItem('topbar', 'Header.MENU_FORMS', 'forms');
        Menus.addMenuItem('topbar', 'Header.MENU_BUILDER', 'formbuilder');
    }
])
.controller('HeaderCtrl', 
    ['$scope', '$translate', '$location', 'Menus', 'RestService', 'ErrorService', 'Authentication', 
    /**
     * [HeaderCtrl - controller for managing header items]
     * @param {[type]} $scope
     * @param {[type]} Menus - service for managing menuItems
     * @param {[type]} Authentication - factory for checking authentication
     */
    function ($scope, $translate, $location, Menus, RestService, ErrorService, Authentication) {
        $scope.currentUser = Authentication.currentUser;
        $scope.isCollapsed = false;

        if (!$scope.currentUser) {
            $location.url('/login');
        }
        $scope.menu = Menus.getMenu('topbar');        

        $scope.sendError = function(currentUser) {
            var data = ErrorService.getInfo();
            console.log(data);
        }

        $scope.changeLanguage = function(key) {
            $translate.use(key);
        }

        /**
         * [toggleCollapsibleMenu - function for toggling header on view changes]
         * @return {[type]}
         */
        $scope.toggleCollapsibleMenu = function() {
            $scope.isCollapsed = !$scope.isCollapsed;
        };        
    }
]);