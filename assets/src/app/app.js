angular.module('dados', [
	'ui.router',
	'ngSails',
	'ngAnimate',
	'ngResource',
	'lodash',
	'ui.sortable',
	'ui.bootstrap',
	'templates-app',
	'pascalprecht.translate',
	'ngTable',

	'services',
	'controllers',
	'dados.auth',
	'dados.header',
	'dados.home',
	'dados.about',
	'dados.users',
	'dados.forms',
	'dados.formbuilder'
])

.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$translateProvider',
	function ($stateProvider, $urlRouterProvider, $locationProvider, $translateProvider) {
		// $urlRouterProvider.otherwise('/home');
		$urlRouterProvider.otherwise(function ($injector, $location) {
			if ($location.$$url === '/') {
				window.location = '/home';
			}
			else {
				// pass through to let the web server handle this request
				window.location = $location.$$absUrl;
			}
		});

		$locationProvider.html5Mode(true);

		$translateProvider.useUrlLoader('/api/locale');
		$translateProvider.preferredLanguage('en');
	}
])
.controller('AppCtrl', ['$scope', 'config', '$modal', '$timeout', '$sails', 
	function ($scope, config, $modal, $timeout, $sails) {
		config.currentUser = window.currentUser;
		var socketErrorModal = null;

		function closeSocketErrorModal() {
			if (socketErrorModal) {
				socketErrorModal.dismiss();
				socketErrorModal = null;
			}
		}

		function openSocketErrorModal(message) {
			closeSocketErrorModal();
			socketErrorModal = $modal.open({
				size: 'sm',
				templateUrl: 'home/error.tpl.html',
				controller: function ErrorModalCtrl($scope, $modalInstance, ErrorService) {
					$scope.error = ErrorService.getInfo();
					$scope.error.message = message;

					$scope.reconnect = function() {
						$sails.socket.connect();
					}
					$scope.sendError = function() {
						console.log($scope.error);
						// ErrorService.getScreenshot();
					}
				},
				backdrop: 'static',
				keyboard: false
			});
		}

		$scope.socketReady = false; // Wait for socket to connect

		$sails.on('connect', function (event, data) {
			closeSocketErrorModal();
			$scope.socketReady = true;
		});

		$sails.on('disconnect', function (event, data) {
			$timeout(function() {
				openSocketErrorModal('The application cannot reach the server... Please wait');
				$scope.socketReady = false;	
			}, 500);			
		});

		$sails.on('failure', function (event, data) {
			openSocketErrorModal('The application failed to connect to the server.');
		});
	}
]);