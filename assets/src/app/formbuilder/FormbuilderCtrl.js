/**
 * Module for FormBuilder page
 */

angular.module('dados.formbuilder', ['ngform-builder'])
.config(['$stateProvider',
    function ($stateProvider) {
        $stateProvider.
        state('formbuilder', {
            url: '/formbuilder',
            views: {
                'main': {
                    controller: 'FormBuilderCtrl',
                    templateUrl: 'formbuilder/formbuilder.tpl.html'
                }
            }
        });
        // state('formbuilder.edit', {
        //     url: '/formbuilder/:formID',
        //     views: {
        //         'main': {
        //             controller: 'FormBuilderCtrl',
        //             templateUrl: 'formbuilder/formbuilder.tpl.html',
        //             resolve: {
        //                 formID: ['$stateParams', function ($stateParams) {
        //                     return $stateParams.formID;
        //                 }]
        //             }
        //         }
        //     }
        // });
    }
])

.controller('FormBuilderCtrl', ['$scope', '$timeout', '$stateParams', 'RestService',
    /**
     * [FormBuilderCtrl]
     * @param {[type]} $scope
     */
    function ($scope, $timeout, $stateParams, RestService) {
        $scope.alerts = [];
        $scope.testForm = {};
        $scope.formID = $stateParams.formID || null;
        console.log($stateParams.formID);

        var addAlert = function(msg) {
            $scope.alerts.push(msg);
            $timeout(function() {
                $scope.alerts.splice(0);
            }, 10000);
        }

        $scope.saveForm = function() {
            RestService.create('form', $scope.testForm, function(err) {
                console.log(err);
                addAlert({msg: err.raw.err, type: 'danger'});
            }).then(function (response) {
                addAlert({msg: 'Saved form successfully!', type: 'success'});
            });
        }

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
    }
]);