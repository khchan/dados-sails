/**
 * Module for Users page:
 * Extends functionality from generic ModelCtrl
 */

angular.module('dados.users', ['nglist-editor'])
.config(['$stateProvider',
    function ($stateProvider) {
        $stateProvider.
        state('users', {
            url: '/users',
            views: {
                'main' : {
                    controller: 'UserCtrl',
                    templateUrl: 'users/user.tpl.html'
                }
            }          
        });
    }
])
.controller('UserCtrl', ['$scope',
    /**
     * [UserCtrl - additional user-specific settings that may be needed]
     * @param {[type]} $scope
     */
    function ($scope, RestService) {
        $scope.model = 'user';
        $scope.modalUrl = 'form?form_name=my_form';
    }
]);