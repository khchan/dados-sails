/**
 * Module for communicating with REST API endpoints;
 * uses low level $http calls that return promises.
 */

angular.module('service.rest', [])
.service('RestService', RestService);

/**
 * [RestService - Exposes REST endpoints with standard CRUD functions]
 * @param {[type]} $q
 * @param {[type]} $http
 * @param {[type]} config
 * @param {[type]} lodash
 * @param {[type]} $sails
 */
function RestService($q, $http, config, lodash, $sails, $resource) {

    /**
     * [getCsrf - simple utility for retrieving csrf token]
     * @return {[type]}
     */
    this.getCsrf = function() {
        var deferred = $q.defer();
        $http.get('/csrfToken').success(function (csrfToken) {
            return deferred.resolve(csrfToken._csrf);
        });
        
        return deferred.promise;
    }

    /**
     * [getAll - retrieves a collection of generic items]
     * @param  {[type]} model - name of item to retrieve
     * @return {[type]} promise
     */
    this.getAll = function(model) {
        var deferred = $q.defer();
        var url = config.apiUrl + model;

        $http.get(url).success(function (model) {
            return deferred.resolve(model);
        });

        return deferred.promise;
    }

    /**
     * [getOne - retrieves an individual generic item]
     * @param  {[type]} model - name of item to retrieve
     * @param  {[type]} id - id of item
     * @return {[type]} promise
     */
    this.getOne = function(model, id) {
        var deferred = $q.defer();
        var url = config.apiUrl + model + '/' + id;

        $http.get(url).success(function (model) {
            return deferred.resolve(model);
        });

        return deferred.promise;
    }

    /**
     * [create - creates a generic item]
     * @param  {[type]}   model - name of item to create
     * @param  {[type]}   newModel - data to pass
     * @param  {Function} next - callback function
     * @return {[type]} promise
     */
    this.create = function(model, newModel, next) {
        var deferred = $q.defer();
        var url = config.apiUrl + model;

        this.getCsrf().then(function (csrfToken) {
            newModel._csrf = csrfToken;
            // post request with added csrf token
            $http.post(url, newModel).success(function (model) {
                return deferred.resolve(model);
            })
            .error(function (err) {
                return next(err);
            });   
        });        

        return deferred.promise;
    }

    /**
     * [update - updates a generic item]
     * @param  {[type]}   model - name of item to update
     * @param  {[type]}   id - id of item
     * @param  {[type]}   newModel - modified data to pass
     * @param  {Function} next - callback function
     * @return {[type]} promise
     */
    this.update = function(model, id, newModel, next) {
        var deferred = $q.defer();
        var url = config.apiUrl + model + '/' + id;
        this.getCsrf().then(function (csrfToken) {
            newModel._csrf = csrfToken;
            $http.put(url, newModel).success(function (model) {
                return deferred.resolve(model);
            })
            .error(function (err) {
                return next(err);
            });
        });  

        return deferred.promise;
    }

    /**
     * [destroy - deletes a generic item]
     * @param  {[type]}   model - name of item to delete
     * @param  {[type]}   id - id of item
     * @param  {Function} next - callback function
     * @return {[type]} promise
     */
    this.destroy = function(model, id, next) {
        var deferred = $q.defer();
        var url = config.apiUrl + model + '/' + id;
        this.getCsrf().then(function (csrfToken) {
            $http.delete(url, csrfToken).success(function (model) {
                return deferred.resolve(model);
            })
            .error(function (err) {
                return next(err);
            });
        });

        return deferred.promise;
    }

    /**
     * [post - custom method for performing requests for non-api resources]
     * @param  {[type]}   url    [custom url]
     * @param  {[type]}   object [parameters to pass]
     * @param  {Function} next   [callback function]
     * @return {[type]}          [description]
     */
    this.post = function(url, object, next) {
        var deferred = $q.defer();
        $http.get('/csrfToken').success(function (csrfToken) {
            object._csrf = csrfToken._csrf;
            // post request with added csrf token
            $http.post(url, object).success(function (obj) {
                return deferred.resolve(obj);
            })
            .error(function (err) {
                console.log('RestService error:');
                console.log(err);
                return next(err);
            });
        });

        return deferred.promise;
    }
}