'use strict';

// Authentication service for user variables
angular.module('service.authentication', [])

.factory('Authentication', [
    function() {
        var _this = this;

        _this._data = {
            currentUser: window.currentUser
        };
        return _this._data;
    }
]);