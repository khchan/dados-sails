angular.module('services', [
	'service.config',
    'service.rest',
    'service.authentication',
	'service.title',
    'service.menu',
    'service.error'
]);