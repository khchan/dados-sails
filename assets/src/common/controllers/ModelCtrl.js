/**
* Module for generic table-based model view templates
* i.e. Users, Forms, etc.
*/

angular.module('controller.modeltable', [])
.controller('ModelCtrl', 
    ['$scope', '$modal', '$filter', 'Authentication', 'RestService', 'ngTableParams',
    /**
     * [ModelCtrl - generic controller for rendering model data to an ngTable
     * Handling of data retrieval is handled by an angular service, RestService]
     * @param {[type]} $scope
     * @param {[type]} $modal
     * @param {[type]} $timeout
     * @param {[type]} $filter
     * @param {[type]} Authentication
     * @param {[type]} RestService
     * @param {[type]} ngTableParams
     */
    function ($scope, $modal, $filter, Authentication, RestService, ngTableParams) {
        $scope.showUserMenu = false;
        $scope.selectedItem = null;
        $scope.currentUser = Authentication.currentUser;
        
        var modal = null
          , data = null;

        // reload our ngTable whenever change is detected
        $scope.$watch('tableParams', function() {
            if ($scope.tableParams && $scope.tableParams.data.length > 0)
                $scope.tableParams.reload();
        });

        var getItems = function(done) {
            // retrieve generic items via RestService and populate table
            RestService.getAll($scope.model)
            .then(function (models) {
                console.log
                $scope.items = models;
                data = $scope.items;

                // auto-select first row in table
                $scope.selectedItem = data[0];
                $scope.selectedItem.$selected = true;
                return done(data);
            });
        }

        var openModal = function(resolve) {
            resolve.model = function() { return $scope.model; }
            resolve.tableParams = function() { return $scope.tableParams; }

            RestService.getAll($scope.modalUrl)
            .then(function (form) {
                modal = $modal.open({
                    templateUrl: form.href,
                    controller: 'ModalInstanceCtrl',
                    size: 'lg',
                    resolve: resolve
                });   
            });
        }

        $scope.tableParams = new ngTableParams({
            page: 1,
            count: 10,
            sorting: {
                first_name: 'asc'
            }
        }, {
            total: function() {
                getItems(function() { return data.length })
            },
            getData: function($defer, params) {
                getItems(function(data) {
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, $scope.tableParams.orderBy()) : data;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * 
                        params.count(), params.page() * params.count()));
                });                
            },
            $scope: { $data: {} }
        });

        /**
         * [createMenu - function for opening createItem modal window]
         * @return {[type]}
         */
        $scope.createMenu = function(form) {
            var resolve = {
                form: function() { return form; },
                item: function() { return null; },
                model: function() { return $scope.model; },
                tableParams: function() { return $scope.tableParams; }
            }
            openModal(resolve);
        }

        /**
         * [editMenu - function for opening editItem modal window]
         * @return {[type]}
         */
        $scope.editMenu = function(form) {
            var resolve = {
                form: function() { return form; },
                item: function() { return $scope.selectedItem; },
                model: function() { return $scope.model; },
                tableParams: function() { return $scope.tableParams; }
            }
            openModal(resolve);
        }

        /**
         * [destroyItem - deletes a generic item via RestService]
         * @param  {[type]} id - id of item
         * @return {[type]}
         */
        $scope.destroyItem = function(id) {
            RestService.destroy($scope.model, id)
            .then(function(model) {
                // do something
            });
        };    

        /**
         * [changeSelection - handling row selection on master table]
         * @param  {json} item [new selected item]
         * @return
         */
        $scope.changeSelection = function(item) {
            // selected different item
            if ($scope.selectedItem.id != item.id) {
                $scope.selectedItem.$selected = false;
                $scope.selectedItem = item;
            }
            // deselect current item
            else if ($scope.selectedItem.item == item.id) {
                $scope.selectedItem = {};
            }
        }
    }
]);