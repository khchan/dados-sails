/**
 * Module for About view
 */

angular.module('dados.about', [])
.config(['$stateProvider', 
	function ($stateProvider) {
		$stateProvider.state('about', {
			url: '/about',
			views: {
				"main": {
					controller: 'AboutCtrl',
					templateUrl: 'about/about.tpl.html'
				}
			}
		});
	}
])
.controller('AboutCtrl', ['$scope', 'titleService',
	function ($scope, titleService) {
		titleService.setTitle('About');
	}
]);