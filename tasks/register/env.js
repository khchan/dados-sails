module.exports = function (grunt) {
    grunt.task.registerTask('env:dev', function() {
        NODE_ENV = 'development';
    });
    grunt.task.registerTask('env:test', function() {
        NODE_ENV = 'test';
    });
    grunt.task.registerTask('env:prod', function() {
        NODE_ENV = 'production';
    });
}