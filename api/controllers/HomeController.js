module.exports = {
	index: function(req, res) {
		res.view({
			title: 'Home',
			currentUser: req.user
		});
	}
};