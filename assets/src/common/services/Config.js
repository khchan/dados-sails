angular.module('service.config', ['lodash'])

.service('config', function(lodash) {
	// private vars here if needed
	return {
		siteName: 'DADOS',
		apiUrl: '/api/',
		currentUser: false
	};
});
