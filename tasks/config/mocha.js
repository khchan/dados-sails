/**
 * Mocha Unit Test Configuration
 */
module.exports = function(grunt) {

    grunt.config.set('mochaTest', {  
        unit: {
            options: {
                reporter: 'spec'
            },
            src: ['tests/**/*.spec.js']
        }
        //, coverage: {
        //     options: {
        //         reporter: 'html-cov',
        //         require: ['should'],
        //         coverage: true,
        //         output: 'reports/coverage.html'
        //     },
        //     files: {
        //         src: 'tests/**/*.spec.js'
        //     }
        // }
    });
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-mocha-cov');
};
