/**
 * Module for Forms page:
 * Extends functionality from generic ModelCtrl
 */

angular.module('dados.forms', ['nglist-editor'])
.config(['$stateProvider',
    function ($stateProvider) {
        $stateProvider.
        state('forms', {
            url: '/forms',
            views: {
                'main' : {
                    controller: 'FormCtrl',
                    templateUrl: 'forms/form.tpl.html'
                }
            }          
        });
    }
])
.controller('FormCtrl', ['$scope', 'RestService',
    /**
     * [FormCtrl - additional form-specific settings that may be needed]
     * @param {[type]} $scope
     */
    function ($scope, RestService) {
        $scope.model = 'form';

        // $scope.list = [];
        // $scope.columns = [];

        // RestService.getAll('form').then(function (forms) {
        //     angular.copy(forms, $scope.list);
        // });
    }
]);